// ==UserScript==
// @name           4chan translator
// @description    Auto translates bullshit code posts
// @namespace      asdf
// @include        http*://boards.4chan.org/*
// @version        0.0.5
// @homepage       http://userscripts.org/scripts/show/f
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_deleteValue
// @grant        GM_listValues
// @grant        GM_openInTab
// @grant        GM_xmlhttpRequest
// @run-at       document-start
// @downloadURL  https://translate.ss13.moe/4chan%20translator.js
// ==/UserScript==
var translated = 0
var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,:?'-/()\"";
var morse = new Array(
".-","-...","-.-.","-..",".","..-.",
"--.","....","..",".---","-.-",".-..",
"--","-.","---",".--.","--.-",".-.",
"...","-","..-","...-",".--","-..-",
"-.--","--..","-----",".----","..---",
"...--","....-",".....","-....","--...",
"---..","----."," ",".-.-.-","--..--",
"---...","..--..",".----.","-....-","-..-.",
"-.--.-","-.--.-",".-..-.");

function replace(string,text,by) {
// Replaces text with by in string
    var strLength = string.length, txtLength = text.length;
    if ((strLength == 0) || (txtLength == 0)) return string;

    var j = string.indexOf(text);
    if ((!j) && (text != string.substring(0,txtLength))) return string;
    if (j == -1) return string;

    var newstr = string.substring(0,j) + by;

    if (j+txtLength < strLength)
        newstr += replace(string.substring(j+txtLength,strLength),text,by);

    return newstr;
}


function translate_back(el){

  var out = el;//.innerHTML;
  out = " " + out + "  ";
  out = replace(out,">", "°~~~");
  out = replace(out,"<", "±~~~");
  out = replace(out,"±~~~br°~~~", " ±~~~br°~~~  ");
  out = replace(out," ", "~~");

  //console.log(out);
  var old = out;
  for (l = 0; l < letters.length; l++){
    if (letters.charAt(l) != " ") {out = replace(out, "~" + morse[l] + "~", "" + letters.charAt(l) + "");}
  }
  out = replace(out," ±~~~br°~~~  ", "±~~~br°~~~");
  out = replace(out, "±~~~", "<");
  out = replace(out, "°~~~", ">");
  out = replace(out, "~~", " ");
  out = replace(out, "~", "");
  out = replace(out, "~~~~", " ");
  return out;//el.innerHTML = out;
}
function scan(){

  translated = 1;
var els = document.getElementsByClassName("postMessage");

var re = new RegExp("[.-]{1,5}(?:[ \t]+[.-]{1,5})*(?:[ \t]+[.-]{1,5}(?:[ \t]+[.-]{1,5})*)");
for(var i = 0, LENGTH = els.length; i < LENGTH; i++) {
  var el = els[i];
  //var inner = el.innerHTML;
  
//console.log("number ", i, " ", Date.now());
it = document.createNodeIterator(el, 4);
while((node = it.nextNode())) {
  //console.log(node.textContent);//node.textContent = '[' + node.textContent + ']';
  var inner = node.textContent;
var matches;
//console.log(inner);
for(var ii = 0; ii < 100; ii++){
    matches = re.exec(inner);

}
  if(matches){
    console.log("regex match on number ", i);
    //alert(matches.length);
    if(matches.length){
      node.textContent = translate_back(inner);
    }
  } //second pass replacing slashes
  else{
    //console.log("no regex match replacing forward slash and rematching");
    inner = inner.replace(/[/]/g, " "); 
    for(var ii = 0; ii < 100; ii++){
		matches = re.exec(inner);

}
  if(matches){
    console.log("regex match on number ", i);
    //alert(matches.length);
    if(matches.length){
      node.textContent = translate_back(inner);//translate_back(el);
    }
  }
  }
  
}
}
}
scan();
function binaryscan(){
  //alert("binary scan");

  translated = 1;
var els = document.getElementsByClassName("postMessage");

var reg = new RegExp("[01]{8,}");
for(var i = 0, LENGTH = els.length; i < LENGTH; i++) {
  var el = els[i];
it = document.createNodeIterator(el, 4);
while((node = it.nextNode())) {
  //console.log(node.textContent);//node.textContent = '[' + node.textContent + ']';
  var inner = node.textContent;
var matches;
//console.log(inner);
for(var ii = 0; ii < 100; ii++){
    matches = reg.exec(inner);

}
  if(matches){
    //console.log("regex match on number ", i);
    //alert(matches.length);
    if(matches.length){
		inner = inner.replace(/([01]+)\s(\d)/g,"$1$2");
        node.textContent = inner.replace(/[01]{8}/g, function(v) { 
    return String.fromCharCode( parseInt(v,2) ); 
});
    }
  } //second pass replacing slashes
  
}
}
}
binaryscan();
function setup() {
  //alert("setup ran");
  var zNode       = document.createElement ('div');
  zNode.innerHTML = '<button id="myButton" type="button">Scan for Translatable Text</button>';
  zNode.setAttribute ('id', 'myContainer');
  zNode.style.position = 'fixed';
  zNode.style.right = '20px';
  zNode.style.top = '60px';
  document.body.appendChild (zNode);


  document.getElementById ("myButton").addEventListener (
    "click", ButtonClickAction, false
);

function ButtonClickAction (zEvent) {

  translated = 0;
  //alert("calling scan()");
  scan();
  binaryscan();
}
  document.getElementById ("binaryButton").addEventListener (
    "click", BinaryClickAction, false
);

function BinaryClickAction (zEvent) {

  translated = 0;
  //alert("calling scan()");
  binaryscan();
}

}
setup();